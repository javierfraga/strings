/* strchr example */
#include <stdio.h>
#include <string.h>
#include <bsd/string.h>

/*#define EOH (const char *)("\\r\\n\\r\\n")*/
#define EOH (const char *)("\r\n\r\n")

int main ()
{
  char str[] = "GETFLE GET /foo.jpg\r\n\r\n"; // 23 chars
  char * pch;
  printf ("Looking for the 's' character in \"%s\"...\n",str);
  pch=strchr(str,' ');
  while (pch!=NULL)
  {
    printf ("found at %d\n",pch-str+1);
    pch=strchr(pch+1,' ');
  }

  /*char header[128] = "GETFILE GET /courses/ud923/filecorpus/paraglider.jpg\\r\\n\\r\\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /courses/ud923/filecorpus/road.jpg\\r\\n\\r\\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /path/to/file.pdf\r\n\r\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /path/to/file.pdf\r\n\r\n"; // 23 chars*/
  char header[128] = "GETFILE GET /foo.jpg\r\n\r\n"; // 23 chars
  /*char header[128] = "GETFILE GET /path/to/file.pdf\r\n\r\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /path/to/file.pdf \r\n\r\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /path /to/ file.pdf \r\n\r\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /path /to/ file.pdf\r\n\r\n"; // 23 chars*/
  /*char header[128] = "GETFILE GET /path\\/to/\\file.pdf \r\n\r\n"; // 23 chars*/
    char scheme[8];
    char method[4];
    char eoh[9];
    char path[256] = {0};
    char *temp;
    char *back_slash;
    char *space;
    int index_end;
    int difference;

    strncpy( scheme, header , 7 );
    scheme[7] = '\0';
    printf("scheme: %s\n", scheme);
    strncpy( method, header+8 , 3 );
    method[3] = '\0';
    printf("method: %s\n", method);
    temp = strchr( header , '/' );
    printf("temp: %s , '/' found at: %d\n", temp , temp-header);
    printf("--------------------------------\n");
    back_slash = strchr( temp , '\r' );
    printf("back_slash: %s , found at: %d , and next char:%c\n", back_slash , back_slash-temp , back_slash[1]);
    strncpy( eoh , back_slash , 8 );
    eoh[8] = '\0';
    /*if ( eoh != EOH ) {*/
    if ( strcmp( eoh , EOH ) != 0 ) {
        do {
            printf("you have another \\ lurking...\n");
            printf("eoh:%s , length:%d\n", eoh , strlen(eoh));
            printf("EOH:%s , length:%d\n", EOH , strlen(EOH));
            back_slash = strchr( back_slash+1 , '\\' );
            strncpy( eoh , back_slash , 8 );
            eoh[8] = '\0';
        } while ( strcmp( eoh , EOH ) != 0 );
    } else {
        printf("all is good with the \\r\n");
        printf("eoh:%s , length:%d\n", eoh , strlen(eoh));
        printf("EOH:%s , length:%d\n", EOH , strlen(EOH));
    }

    /*do {*/
        
    /*} while ( space != NULL );*/
    space = strchr( temp , ' ' );
    printf("space: %s , found at: %d\n", space , space-temp);
    if ( space != NULL ) {
        if ( (difference = back_slash-space) > 0 ) { // space before '\r\n\r\n'
            printf("there are too many spaces:%d\n", difference);
            if ( difference == 1 ) {
                printf("space in right place and only one\n");
                index_end = space - temp;
            } else {
                printf("space in path!!\n");
                printf("space:%s\n", space);
                printf("next char after spacespace:%c\n", space[1]);
                int i = 0;
                while ( (space != NULL) && (space[1] != '\\') ) {
                    printf("found %d space in path\n", ++i);
                    space = strchr( space+1 , ' ' );
                }
                if ( space == NULL ) {
                    index_end = back_slash - temp;
                } else {
                    index_end = space - temp;
                }
            }
        } else { // space after '\r\n\r\n'
            printf("there are too many spaces:%d\n", difference);
            index_end = back_slash - temp;
        }
        printf("space-back_slash: %d\n", space-back_slash);
        printf("back_slash-space: %d\n", back_slash-space);
    } else {
        index_end = back_slash - temp;
    }
    printf("index_end: %d\n", index_end);
    strncpy( path , temp , index_end );
    printf("path:%s\n", path);
    printf("path_length:%d\n", strlen(path));

  return 0;
}
