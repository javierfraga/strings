CFLAGS=-Wall -g3
all:
	make ./snprintf
	make ./strncpy
	make ./strchr
	make ./ascii
	make ./strstr
	make ./strtok_r
	make ./fscanf
clean:
	rm -f snprintf strncpy strchr ascii strstr strtok_r fscanf
