/* strstr example */
#include <stdio.h>
#include <string.h>

int main ()
{
  char str[] ="This is a simple string";
  char * pch;
  pch = strstr (str,"simple");
  puts (pch);
  strncpy (pch,"sample",6);
  puts (pch);
  puts (str);
  return 0;
}
