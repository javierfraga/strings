#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[])
{
/*
 *https://linux.die.net/man/3/strtok_r
 */
    /*char *str1, *str2, *token, *subtoken;*/
    /*char *saveptr1, *saveptr2;*/
    /*int j;*/

   /*if (argc != 4) {*/
        /*fprintf(stderr, "Usage: %s string delim subdelim\n",*/
                /*argv[0]);*/
        /*exit(EXIT_FAILURE);*/
    /*}*/

   /*for (j = 1, str1 = argv[1]; ; j++, str1 = NULL) {*/
        /*token = strtok_r(str1, argv[2], &saveptr1);*/
        /*if (token == NULL)*/
            /*break;*/
        /*printf("%d: %s\n", j, token);*/

       /*for (str2 = token; ; str2 = NULL) {*/
            /*subtoken = strtok_r(str2, argv[3], &saveptr2);*/
            /*if (subtoken == NULL)*/
                /*break;*/
            /*printf(" --> %s\n", subtoken);*/
        /*}*/
    /*}*/


/*
 *[>http://stackoverflow.com/questions/15961253/c-correct-usage-of-strtok-r<]
 */
    char str[] = "Hello world guys";
    char *saveptr,*saveptr2;
    char *foo, *bar, *foobar;

    foo = strtok_r(str, " ", &saveptr);
    bar = strtok_r(NULL, " ", &saveptr);
    foobar = strtok_r(NULL, " ", &saveptr);

    printf("foo:%s\n", foo);
    printf("bar:%s\n", bar);
    printf("foobar:%s\n", foobar);

    printf("=======================\n");

    foo = strtok_r(str, " ", &saveptr2);
    bar = strtok_r(NULL, " ", &saveptr2);
    foobar = strtok_r(NULL, " ", &saveptr2);

    printf("foo:%s\n", foo);
    printf("bar:%s\n", bar);
    printf("foobar:%s\n", foobar);


   exit(EXIT_SUCCESS);
}
