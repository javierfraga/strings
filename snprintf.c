/* snprintf example */
#include <stdio.h>
#include <string.h>

int main ()
{
    char buffer [100];
    int cx;
    int cx2;

    cx = snprintf ( buffer, 100, "The half of %d is %d", 60, 60/2 );
    printf("strlen: %d , cx:%d\n", strlen(buffer), cx);

    if (cx>=0 && cx<100)      // check returned value
        cx2 = snprintf ( buffer+cx, 100-cx, ", and the half of that is %d.", 60/2/2 );

    puts (buffer);
    printf("strlen: %d , cx2:%d\n", strlen(buffer), cx2);

    return 0;
}
